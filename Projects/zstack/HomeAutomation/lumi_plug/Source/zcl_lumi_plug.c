/**************************************************************************************************
  Filename:       zcl_sampleLight.c
  Revised:        $Date: 2014-10-24 16:04:46 -0700 (Fri, 24 Oct 2014) $
  Revision:       $Revision: 40796 $


  Description:    Zigbee Cluster Library - sample light application.


  Copyright 2006-2014 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/*********************************************************************
  This application implements a ZigBee Light, based on Z-Stack 3.0. It can be configured as an
  On/Off light or as a dimmable light, by undefining or defining ZCL_LEVEL_CTRL, respectively.

  This application is based on the common sample-application user interface. Please see the main
  comment in zcl_sampleapp_ui.c. The rest of this comment describes only the content specific for
  this sample applicetion.

  Application-specific UI peripherals being used:

  - LEDs:
    LED1 reflect the current light state (On / Off accordingly).

  Application-specific menu system:

    <TOGGLE LIGHT> Toggle the local light and display its status and level
      Press OK to toggle the local light on and off.
      This screen shows the following information
        Line1: (only populated if ZCL_LEVEL_CTRL is defined)
          LEVEL XXX - xxx is the current level of the light if the light state is ON, or the target level
            of the light when the light state is off. The target level is the level that the light will be
            set to when it is switched from off to on using the on or the toggle commands.
        Line2:
          LIGHT OFF / ON: shows the current state of the light.
      Note when ZCL_LEVEL_CTRL is enabled:
        - If the light state is ON and the light level is X, and then the light receives the OFF or TOGGLE
          commands: The level will decrease gradually until it reaches 1, and only then the light state will
          be changed to OFF. The level then will be restored to X, with the state staying OFF. At this stage
          the light is not lighting, and the level represent the target level for the next ON or TOGGLE
          commands.
        - If the light state is OFF and the light level is X, and then the light receives the ON or TOGGLE
          commands; The level will be set to 1, the light state will be set to ON, and then the level will
          increase gradually until it reaches level X.
        - Any level-setting command will affect the level directly, and may also affect the on/off state,
          depending on the command's arguments.

*********************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "ZComDef.h"
#include "OSAL.h"
#include "AF.h"
#include "ZDApp.h"
#include "ZDObject.h"
#include "MT_SYS.h"

#include "nwk_util.h"

#include "zcl.h"
#include "zcl_general.h"
#include "zcl_ha.h"
#include "zcl_diagnostic.h"

#include "zcl_lumi_plug.h"

#include "bdb.h"
#include "bdb_interface.h"

//GP_UPDATE
#include "gp_interface.h"

#include "onboard.h"

/* HAL */
#include "hal_lcd.h"
#include "hal_led.h"
#include "hal_key.h"

#include "NLMEDE.h"

// Added to include TouchLink initiator functionality
#if defined ( BDB_TL_INITIATOR )
#include "bdb_touchlink_initiator.h"
#endif // BDB_TL_INITIATOR

#if defined ( BDB_TL_TARGET )
#include "bdb_touchlink_target.h"
#endif // BDB_TL_TARGET

#if defined ( BDB_TL_INITIATOR ) || defined ( BDB_TL_TARGET )
#include "bdb_touchlink.h"
#endif

#include "zcl_sampleapps_ui.h"

/*********************************************************************
 * MACROS
 */
#define UI_STATE_TOGGLE_LIGHT 1 //UI_STATE_BACK_FROM_APP_MENU is item #0, so app item numbers should start from 1

#define APP_TITLE "TI Sample Light"

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
byte zcl_lumi_plug_TaskID;
uint8 zclSampleLightSeqNum;

/*********************************************************************
 * GLOBAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
afAddrType_t zcl_lumi_plug_DstAddr;

// Test Endpoint to allow SYS_APP_MSGs
static endPointDesc_t sampleLight_TestEp = {
    LUMI_PLUG_ENDPOINT,
    0,
    &zcl_lumi_plug_TaskID,
    (SimpleDescriptionFormat_t *)NULL,  // No Simple description for this test endpoint
    (afNetworkLatencyReq_t)0            // No Network Latency req
};

#ifdef ZCL_LEVEL_CTRL
uint8 zcl_lumi_plug_WithOnOff;       // set to TRUE if state machine should set light on/off
uint8 zcl_lumi_plug_NewLevel;        // new level when done moving
uint8 zcl_lumi_plug_LevelChangeCmd; // current level change was triggered by an on/off command
bool  zcl_lumi_plug_NewLevelUp;      // is direction to new level up or down?
int32 zcl_lumi_plug_CurrentLevel32;  // current level, fixed point (e.g. 192.456)
int32 zcl_lumi_plug_Rate32;          // rate in units, fixed point (e.g. 16.123)
uint8 zcl_lumi_plug_LevelLastLevel;  // to save the Current Level before the light was turned OFF
#endif

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void zcl_lumi_plug_HandleKeys( byte shift, byte keys );
static void zcl_lumi_plug_BasicResetCB( void );
static void zcl_lumi_plug_OnOffCB( uint8 cmd );
//GP_UPDATE
#if (ZG_BUILD_RTR_TYPE)
static void gp_CommissioningMode(bool isEntering);
static uint8 gp_ChangeChannelReq(void);
#endif


static void zcl_lumi_plug_ProcessCommissioningStatus(bdbCommissioningModeMsg_t *bdbCommissioningModeMsg);


#ifdef ZCL_LEVEL_CTRL
static void zcl_lumi_plug_LevelControlMoveToLevelCB( zclLCMoveToLevel_t *pCmd );
static void zcl_lumi_plug_LevelControlMoveCB( zclLCMove_t *pCmd );
static void zcl_lumi_plug_LevelControlStepCB( zclLCStep_t *pCmd );
static void zcl_lumi_plug_LevelControlStopCB( void );
static void zcl_lumi_plug_DefaultMove( uint8 OnOff );
static uint32 zcl_lumi_plug_TimeRateHelper( uint8 newLevel );
static uint16 zcl_lumi_plug_GetTime ( uint8 level, uint16 time );
static void zcl_lumi_plug_MoveBasedOnRate( uint8 newLevel, uint32 rate );
static void zcl_lumi_plug_MoveBasedOnTime( uint8 newLevel, uint16 time );
static void zcl_lumi_plug_AdjustLightLevel( void );
#endif

// Functions to process ZCL Foundation incoming Command/Response messages
static void zcl_lumi_plug_ProcessIncomingMsg( zclIncomingMsg_t *msg );
#ifdef ZCL_READ
static uint8 zcl_lumi_plug_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg );
#endif
#ifdef ZCL_WRITE
static uint8 zcl_lumi_plug_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg );
#endif
static uint8 zcl_lumi_plug_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg );
#ifdef ZCL_DISCOVER
static uint8 zcl_lumi_plug_ProcessInDiscCmdsRspCmd( zclIncomingMsg_t *pInMsg );
static uint8 zcl_lumi_plug_ProcessInDiscAttrsRspCmd( zclIncomingMsg_t *pInMsg );
static uint8 zcl_lumi_plug_ProcessInDiscAttrsExtRspCmd( zclIncomingMsg_t *pInMsg );
#endif

static void zclSampleApp_BatteryWarningCB( uint8 voltLevel);

void zcl_lumi_plug_UiActionToggleLight(uint16 keys);
void zcl_lumi_plug_UiUpdateLcd(uint8 uiCurrentState, char * line[3]);
void zcl_lumi_plug_UpdateLedState(void);

/*********************************************************************
 * CONSTANTS
 */
const uiState_t zcl_lumi_plug_UiStatesMain[] = {
    /*  UI_STATE_BACK_FROM_APP_MENU  */   {UI_STATE_DEFAULT_MOVE,       UI_STATE_TOGGLE_LIGHT,  UI_KEY_SW_5_PRESSED, &UI_ActionBackFromAppMenu}, //do not change this line, except for the second item, which should point to the last entry in this menu
    /*  UI_STATE_TOGGLE_LIGHT        */   {UI_STATE_BACK_FROM_APP_MENU, UI_STATE_DEFAULT_MOVE,  UI_KEY_SW_5_PRESSED, &zcl_lumi_plug_UiActionToggleLight},
};

#define LEVEL_CHANGED_BY_LEVEL_CMD  0
#define LEVEL_CHANGED_BY_ON_CMD     1
#define LEVEL_CHANGED_BY_OFF_CMD    2

/*********************************************************************
 * STATUS STRINGS
 */
#ifdef LCD_SUPPORTED
const char sLightOn[]      = "   LIGHT ON     ";
const char sLightOff[]     = "   LIGHT OFF    ";
#ifdef ZCL_LEVEL_CTRL
char sLightLevel[]        = "   LEVEL ###    "; // displays level 1-254
#endif
#endif

/*********************************************************************
 * REFERENCED EXTERNALS
 */
extern int16 zdpExternalStateTaskID;

/*********************************************************************
 * ZCL General Profile Callback table
 */
static zclGeneral_AppCallbacks_t zcl_lumi_plug_CmdCallbacks = {
    zcl_lumi_plug_BasicResetCB,            // Basic Cluster Reset command
    NULL,                                   // Identify Trigger Effect command
    zcl_lumi_plug_OnOffCB,                 // On/Off cluster commands
    NULL,                                   // On/Off cluster enhanced command Off with Effect
    NULL,                                   // On/Off cluster enhanced command On with Recall Global Scene
    NULL,                                   // On/Off cluster enhanced command On with Timed Off
#ifdef ZCL_LEVEL_CTRL
    zcl_lumi_plug_LevelControlMoveToLevelCB, // Level Control Move to Level command
    zcl_lumi_plug_LevelControlMoveCB,        // Level Control Move command
    zcl_lumi_plug_LevelControlStepCB,        // Level Control Step command
    zcl_lumi_plug_LevelControlStopCB,        // Level Control Stop command
#endif
#ifdef ZCL_GROUPS
    NULL,                                   // Group Response commands
#endif
#ifdef ZCL_SCENES
    NULL,                                  // Scene Store Request command
    NULL,                                  // Scene Recall Request command
    NULL,                                  // Scene Response command
#endif
#ifdef ZCL_ALARMS
    NULL,                                  // Alarm (Response) commands
#endif
#ifdef SE_UK_EXT
    NULL,                                  // Get Event Log command
    NULL,                                  // Publish Event Log command
#endif
    NULL,                                  // RSSI Location command
    NULL                                   // RSSI Location Response command
};

/*********************************************************************
 * @fn          zcl_lumi_plug_Init
 *
 * @brief       Initialization function for the zclGeneral layer.
 *
 * @param       none
 *
 * @return      none
 */
void zcl_lumi_plug_Init( byte task_id ) {
    zcl_lumi_plug_TaskID = task_id;

    // Set destination address to indirect
    zcl_lumi_plug_DstAddr.addrMode = (afAddrMode_t)AddrNotPresent;
    zcl_lumi_plug_DstAddr.endPoint = 0;
    zcl_lumi_plug_DstAddr.addr.shortAddr = 0;

    // Register the Simple Descriptor for this application
    bdb_RegisterSimpleDescriptor( &zcl_lumi_plug_SimpleDesc );

    // Register the ZCL General Cluster Library callback functions
    zclGeneral_RegisterCmdCallbacks( LUMI_PLUG_ENDPOINT, &zcl_lumi_plug_CmdCallbacks );

    // Register the application's attribute list
    zcl_lumi_plug_ResetAttributesToDefaultValues();
    zcl_registerAttrList( LUMI_PLUG_ENDPOINT, zcl_lumi_plug_NumAttributes, zcl_lumi_plug_Attrs );

#ifdef ZCL_LEVEL_CTRL
    zcl_lumi_plug_LevelLastLevel = zcl_lumi_plug_LevelCurrentLevel;
#endif

    // Register the Application to receive the unprocessed Foundation command/response messages
    zcl_registerForMsg( zcl_lumi_plug_TaskID );

#ifdef ZCL_DISCOVER
    // Register the application's command list
    zcl_registerCmdList( LUMI_PLUG_ENDPOINT, zclCmdsArraySize, zcl_lumi_plug_Cmds );
#endif

    // Register low voltage NV memory protection application callback
    RegisterVoltageWarningCB( zclSampleApp_BatteryWarningCB );

    // Register for all key events - This app will handle all key events
    RegisterForKeys( zcl_lumi_plug_TaskID );

    bdb_RegisterCommissioningStatusCB( zcl_lumi_plug_ProcessCommissioningStatus );

    // Register for a test endpoint
//    afRegister( &sampleLight_TestEp );

#ifdef ZCL_DIAGNOSTIC
    // Register the application's callback function to read/write attribute data.
    // This is only required when the attribute data format is unknown to ZCL.
    zcl_registerReadWriteCB( LUMI_PLUG_ENDPOINT, zclDiagnostic_ReadWriteAttrCB, NULL );

    if ( zclDiagnostic_InitStats() == ZSuccess ) {
        // Here the user could start the timer to save Diagnostics to NV
    }
#endif

//GP_UPDATE
#if (ZG_BUILD_RTR_TYPE)
    gp_RegisterCommissioningModeCB(gp_CommissioningMode);
    gp_RegisterGPChangeChannelReqCB(gp_ChangeChannelReq);
#endif

    zdpExternalStateTaskID = zcl_lumi_plug_TaskID;

    UI_Init(zcl_lumi_plug_TaskID, SAMPLEAPP_LCD_AUTO_UPDATE_EVT, SAMPLEAPP_KEY_AUTO_REPEAT_EVT, &zcl_lumi_plug_IdentifyTime, APP_TITLE, &zcl_lumi_plug_UiUpdateLcd, zcl_lumi_plug_UiStatesMain);

    UI_UpdateLcd();
}

/*********************************************************************
 * @fn          zclSample_event_loop
 *
 * @brief       Event Loop Processor for zclGeneral.
 *
 * @param       none
 *
 * @return      none
 */
uint16 zcl_lumi_plug_event_loop( uint8 task_id, uint16 events ) {
    afIncomingMSGPacket_t *MSGpkt;

    (void)task_id;  // Intentionally unreferenced parameter

    if ( events & SYS_EVENT_MSG ) {
        while ( (MSGpkt = (afIncomingMSGPacket_t *)osal_msg_receive( zcl_lumi_plug_TaskID )) ) {
            switch ( MSGpkt->hdr.event ) {
            case ZCL_INCOMING_MSG:
                // Incoming ZCL Foundation command/response messages
                zcl_lumi_plug_ProcessIncomingMsg( (zclIncomingMsg_t *)MSGpkt );
                break;

            case KEY_CHANGE:
                zcl_lumi_plug_HandleKeys( ((keyChange_t *)MSGpkt)->state, ((keyChange_t *)MSGpkt)->keys );
                break;

            case ZDO_STATE_CHANGE:
                UI_DeviceStateUpdated((devStates_t)(MSGpkt->hdr.status));
                break;

            default:
                break;
            }

            // Release the memory
            osal_msg_deallocate( (uint8 *)MSGpkt );
        }

        // return unprocessed events
        return (events ^ SYS_EVENT_MSG);
    }

#ifdef ZCL_LEVEL_CTRL
    if ( events & SAMPLELIGHT_LEVEL_CTRL_EVT ) {
        zcl_lumi_plug_AdjustLightLevel();
        return ( events ^ SAMPLELIGHT_LEVEL_CTRL_EVT );
    }
#endif

#if ZG_BUILD_ENDDEVICE_TYPE
    if ( events & SAMPLEAPP_END_DEVICE_REJOIN_EVT ) {
        bdb_ZedAttemptRecoverNwk();
        return ( events ^ SAMPLEAPP_END_DEVICE_REJOIN_EVT );
    }
#endif

    if ( events & SAMPLEAPP_LCD_AUTO_UPDATE_EVT ) {
        UI_UpdateLcd();
        return ( events ^ SAMPLEAPP_LCD_AUTO_UPDATE_EVT );
    }

    if ( events & SAMPLEAPP_KEY_AUTO_REPEAT_EVT ) {
        UI_MainStateMachine(UI_KEY_AUTO_PRESSED);
        return ( events ^ SAMPLEAPP_KEY_AUTO_REPEAT_EVT );
    }

    // Discard unknown events
    return 0;
}


/*********************************************************************
 * @fn      zcl_lumi_plug_HandleKeys
 *
 * @brief   Handles all key events for this device.
 *
 * @param   shift - true if in shift/alt.
 * @param   keys - bit field for key events. Valid entries:
 *                 HAL_KEY_SW_5
 *                 HAL_KEY_SW_4
 *                 HAL_KEY_SW_2
 *                 HAL_KEY_SW_1
 *
 * @return  none
 */

#define DEFAULT_COMISSIONING_MODE (BDB_COMMISSIONING_MODE_NWK_STEERING | BDB_COMMISSIONING_MODE_NWK_FORMATION)

static void zcl_lumi_plug_HandleKeys( byte shift, byte current_keys ) {
    //argument is uint16 to allow 8 bits for the keys (for future use), and another unique value to mark a re-press of the previous keys.
    static byte PrevKeys = 0; //holds the keys that were pressed during the previous time this function was called. (Does not hold the keys that were released.)
    uint16 keys; //will hold the bitmask of the currently pressed keys at the lower 8 bits, and the keys that have just been released at the higher 8 bits.

    if (current_keys == UI_KEY_AUTO_PRESSED) {
        keys = PrevKeys;
    } else {
        keys = (current_keys | (((PrevKeys ^ current_keys) & PrevKeys) << 8));
    }
    //right key(pcb mark s2)
    if (keys & UI_KEY_SW_2_PRESSED) {
        bdb_StartCommissioning(DEFAULT_COMISSIONING_MODE);
    }
    //center key (pcb mark s1)
    if (keys & UI_KEY_SW_5_PRESSED) {
        zcl_lumi_plug_UiActionToggleLight(keys);
    }
    PrevKeys = keys & 0xFF; //only remember the keys that are currently pressed. Released keys are irrelevant. the 0xFF is not needed, since the target is uint8, but it is there just so it is clear that this assignment of uint16 into uint8 is intentional.
}

//GP_UPDATE
#if (ZG_BUILD_RTR_TYPE)
/*********************************************************************
 * @fn      gp_CommissioningMode
 *
 * @brief   Callback that notifies the application that gp Proxy is entering
 *          into commissioning mode
 *
 * @param   isEntering -
 *
 * @return
 */
static void gp_CommissioningMode(bool isEntering) {
    if(isEntering) {
        //Led on indicating enter commissioning mode
    } else {
        //Led off indicating enter commissioning mode
    }
}



//GP_UPDATE
/*********************************************************************
 * @fn      gp_ChangeChannelReq
 *
 * @brief   Callback function to notify the application about a GP commissioning
 * request that will change the current channel for at most
 * gpBirectionalCommissioningChangeChannelTimeout ms
 *
 * @param   channel - Channel in which the commissioning will take place
 *
 * @return  TRUE to allow change channel, FALSE to do not allow
 */
static uint8 gp_ChangeChannelReq(void) {
    bool allowChangeChannel = TRUE;

    //Check application state to decide if allow change channel or not

    return allowChangeChannel;
}

#endif

static void zcl_default_report_attr(void) {
    uint8 i,state;
    afAddrType_t dstAddr;
    zclReportCmd_t *pReportCmd;
    // actually send the report
    if( 2) {
        dstAddr.addrMode = (afAddrMode_t)Addr16Bit;
        dstAddr.addr.shortAddr = 0;
        dstAddr.endPoint = LUMI_PLUG_ENDPOINT;
        dstAddr.panId = _NIB.nwkPanId;
        pReportCmd = osal_mem_alloc( sizeof( zclReportCmd_t ) + (2 * sizeof( zclReport_t )) );
        if ( pReportCmd != NULL ) {
            pReportCmd->numAttr = 2;
            for ( i = 0; i < 2; ++ i ) {
                pReportCmd->attrList[i].attrID	 = 0xFFFF;
                pReportCmd->attrList[i].dataType = 0xFF;
                pReportCmd->attrList[i].attrData = NULL;
                zclAttrRec_t attr_rec;
                if(i==0) {
                    state=zclFindAttrRec(LUMI_PLUG_ENDPOINT,ZCL_CLUSTER_ID_GEN_BASIC,ATTRID_BASIC_MODEL_ID,&attr_rec);
                } else if (i==1) {
                    state=zclFindAttrRec(LUMI_PLUG_ENDPOINT,ZCL_CLUSTER_ID_GEN_BASIC,ATTRID_BASIC_APPL_VERSION,&attr_rec);
                }
                if(state) {
                    zclAttribute_t attr=attr_rec.attr;
                    pReportCmd->attrList[i].attrID = attr.attrId;
                    pReportCmd->attrList[i].dataType = attr.dataType;
                    pReportCmd->attrList[i].attrData = attr.dataPtr;
                }
            }
            ZStatus_t status=zcl_SendReportCmd( LUMI_PLUG_ENDPOINT, &dstAddr,
                                                ZCL_CLUSTER_ID_GEN_BASIC, pReportCmd,
                                                ZCL_FRAME_SERVER_CLIENT_DIR, BDB_REPORTING_DISABLE_DEFAULT_RSP, bdb_getZCLFrameCounter() );
            osal_mem_free( pReportCmd );
        }

    }

}
void bdb_RepStartOrContinueReporting( void );
/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessCommissioningStatus
 *
 * @brief   Callback in which the status of the commissioning process are reported
 *
 * @param   bdbCommissioningModeMsg - Context message of the status of a commissioning process
 *
 * @return  none
 */
static void zcl_lumi_plug_ProcessCommissioningStatus(bdbCommissioningModeMsg_t *bdbCommissioningModeMsg) {
    switch(bdbCommissioningModeMsg->bdbCommissioningMode) {
    case BDB_COMMISSIONING_FORMATION:
        if(bdbCommissioningModeMsg->bdbCommissioningStatus == BDB_COMMISSIONING_SUCCESS) {
            //After formation, perform nwk steering again plus the remaining commissioning modes that has not been process yet
            bdb_StartCommissioning(BDB_COMMISSIONING_MODE_NWK_STEERING | bdbCommissioningModeMsg->bdbRemainingCommissioningModes);
        } else {
            //Want to try other channels?
            //try with bdb_setChannelAttribute
        }
        break;
    case BDB_COMMISSIONING_NWK_STEERING:
        if(bdbCommissioningModeMsg->bdbCommissioningStatus == BDB_COMMISSIONING_SUCCESS) {
            //YOUR JOB:
            //We are on the nwk, what now?
            //zclFindAttrRec();
            //zcl_SendReportCmd();
//			zcl_default_report_attr();
            zcl_default_report_attr();
			bdb_RepAddAttrCfgRecordDefaultToList(LUMI_PLUG_ENDPOINT,ZCL_CLUSTER_ID_GEN_BASIC,0xFF01,8,10,0);
			bdb_RepStartOrContinueReporting();
        } else {
            //See the possible errors for nwk steering procedure
            //No suitable networks found
            //Want to try other channels?
            //try with bdb_setChannelAttribute
        }
        break;
    case BDB_COMMISSIONING_FINDING_BINDING:
        if(bdbCommissioningModeMsg->bdbCommissioningStatus == BDB_COMMISSIONING_SUCCESS) {
            //YOUR JOB:
        } else {
            //YOUR JOB:
            //retry?, wait for user interaction?
        }
        break;
    case BDB_COMMISSIONING_INITIALIZATION:
        //Initialization notification can only be successful. Failure on initialization
        //only happens for ZED and is notified as BDB_COMMISSIONING_PARENT_LOST notification

        //YOUR JOB:
        //We are on a network, what now?

        break;
#if ZG_BUILD_ENDDEVICE_TYPE
    case BDB_COMMISSIONING_PARENT_LOST:
        if(bdbCommissioningModeMsg->bdbCommissioningStatus == BDB_COMMISSIONING_NETWORK_RESTORED) {
            //We did recover from losing parent
        } else {
            //Parent not found, attempt to rejoin again after a fixed delay
            osal_start_timerEx(zcl_lumi_plug_TaskID, SAMPLEAPP_END_DEVICE_REJOIN_EVT, SAMPLEAPP_END_DEVICE_REJOIN_DELAY);
        }
        break;
#endif
    }

    UI_UpdateComissioningStatus(bdbCommissioningModeMsg);
}

/*********************************************************************
 * @fn      zcl_lumi_plug_BasicResetCB
 *
 * @brief   Callback from the ZCL General Cluster Library
 *          to set all the Basic Cluster attributes to default values.
 *
 * @param   none
 *
 * @return  none
 */
static void zcl_lumi_plug_BasicResetCB( void ) {
    //Reset every attribute in all supported cluster to their default value.

    zcl_lumi_plug_ResetAttributesToDefaultValues();

    zcl_lumi_plug_UpdateLedState();

    // update the display
    UI_UpdateLcd( );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_OnOffCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received an On/Off Command for this application.
 *
 * @param   cmd - COMMAND_ON, COMMAND_OFF or COMMAND_TOGGLE
 *
 * @return  none
 */
static void zcl_lumi_plug_OnOffCB( uint8 cmd ) {
    afIncomingMSGPacket_t *pPtr = zcl_getRawAFMsg();

    uint8 OnOff;

    zcl_lumi_plug_DstAddr.addr.shortAddr = pPtr->srcAddr.addr.shortAddr;


    // Turn on the light
    if ( cmd == COMMAND_ON ) {
        OnOff = LIGHT_ON;
    }
    // Turn off the light
    else if ( cmd == COMMAND_OFF ) {
        OnOff = LIGHT_OFF;
    }
    // Toggle the light
    else if ( cmd == COMMAND_TOGGLE ) {
#ifdef ZCL_LEVEL_CTRL
        if (zcl_lumi_plug_LevelRemainingTime > 0) {
            if (zcl_lumi_plug_NewLevelUp) {
                OnOff = LIGHT_OFF;
            } else {
                OnOff = LIGHT_ON;
            }
        } else {
            if (zcl_lumi_plug_OnOff == LIGHT_ON) {
                OnOff = LIGHT_OFF;
            } else {
                OnOff = LIGHT_ON;
            }
        }
#else
        if (zcl_lumi_plug_OnOff == LIGHT_ON) {
            OnOff = LIGHT_OFF;
        } else {
            OnOff = LIGHT_ON;
        }
#endif
    }

#ifdef ZCL_LEVEL_CTRL
    zcl_lumi_plug_LevelChangeCmd = (OnOff == LIGHT_ON ? LEVEL_CHANGED_BY_ON_CMD : LEVEL_CHANGED_BY_OFF_CMD);

    zcl_lumi_plug_DefaultMove(OnOff);
#else
    zcl_lumi_plug_OnOff = OnOff;
#endif

    zcl_lumi_plug_UpdateLedState();

    // update the display
    UI_UpdateLcd( );
}

#ifdef ZCL_LEVEL_CTRL
/*********************************************************************
 * @fn      zcl_lumi_plug_TimeRateHelper
 *
 * @brief   Calculate time based on rate, and startup level state machine
 *
 * @param   newLevel - new level for current level
 *
 * @return  diff (directly), zcl_lumi_plug_CurrentLevel32 and zcl_lumi_plug_NewLevel, zcl_lumi_plug_NewLevelUp
 */
static uint32 zcl_lumi_plug_TimeRateHelper( uint8 newLevel ) {
    uint32 diff;
    uint32 newLevel32;

    // remember current and new level
    zcl_lumi_plug_NewLevel = newLevel;
    zcl_lumi_plug_CurrentLevel32 = (uint32)1000 * zcl_lumi_plug_LevelCurrentLevel;

    // calculate diff
    newLevel32 = (uint32)1000 * newLevel;
    if ( zcl_lumi_plug_LevelCurrentLevel > newLevel ) {
        diff = zcl_lumi_plug_CurrentLevel32 - newLevel32;
        zcl_lumi_plug_NewLevelUp = FALSE;  // moving down
    } else {
        diff = newLevel32 - zcl_lumi_plug_CurrentLevel32;
        zcl_lumi_plug_NewLevelUp = TRUE;   // moving up
    }

    return ( diff );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_MoveBasedOnRate
 *
 * @brief   Calculate time based on rate, and startup level state machine
 *
 * @param   newLevel - new level for current level
 * @param   rate16   - fixed point rate (e.g. 16.123)
 *
 * @return  none
 */
static void zcl_lumi_plug_MoveBasedOnRate( uint8 newLevel, uint32 rate ) {
    uint32 diff;

    // determine how much time (in 10ths of seconds) based on the difference and rate
    zcl_lumi_plug_Rate32 = rate;
    diff = zcl_lumi_plug_TimeRateHelper( newLevel );
    zcl_lumi_plug_LevelRemainingTime = diff / rate;
    if ( !zcl_lumi_plug_LevelRemainingTime ) {
        zcl_lumi_plug_LevelRemainingTime = 1;
    }

    osal_start_timerEx( zcl_lumi_plug_TaskID, SAMPLELIGHT_LEVEL_CTRL_EVT, 100 );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_MoveBasedOnTime
 *
 * @brief   Calculate rate based on time, and startup level state machine
 *
 * @param   newLevel  - new level for current level
 * @param   time      - in 10ths of seconds
 *
 * @return  none
 */
static void zcl_lumi_plug_MoveBasedOnTime( uint8 newLevel, uint16 time ) {
    uint16 diff;

    // determine rate (in units) based on difference and time
    diff = zcl_lumi_plug_TimeRateHelper( newLevel );
    zcl_lumi_plug_LevelRemainingTime = zcl_lumi_plug_GetTime( newLevel, time );
    zcl_lumi_plug_Rate32 = diff / time;

    osal_start_timerEx( zcl_lumi_plug_TaskID, SAMPLELIGHT_LEVEL_CTRL_EVT, 100 );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_GetTime
 *
 * @brief   Determine amount of time that MoveXXX will take to complete.
 *
 * @param   level = new level to move to
 *          time  = 0xffff=default, or 0x0000-n amount of time in tenths of seconds.
 *
 * @return  none
 */
static uint16 zcl_lumi_plug_GetTime( uint8 newLevel, uint16 time ) {
    // there is a hiearchy of the amount of time to use for transistioning
    // check each one in turn. If none of defaults are set, then use fastest
    // time possible.
    if ( time == 0xFFFF ) {
        // use On or Off Transition Time if set (not 0xffff)
        if ( zcl_lumi_plug_LevelCurrentLevel > newLevel ) {
            time = zcl_lumi_plug_LevelOffTransitionTime;
        } else {
            time = zcl_lumi_plug_LevelOnTransitionTime;
        }

        // else use OnOffTransitionTime if set (not 0xffff)
        if ( time == 0xFFFF ) {
            time = zcl_lumi_plug_LevelOnOffTransitionTime;
        }

        // else as fast as possible
        if ( time == 0xFFFF ) {
            time = 1;
        }
    }

    if ( time == 0 ) {
        time = 1; // as fast as possible
    }

    return ( time );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_DefaultMove
 *
 * @brief   We were turned on/off. Use default time to move to on or off.
 *
 * @param   zcl_lumi_plug_OnOff - must be set prior to calling this function.
 *
 * @return  none
 */
static void zcl_lumi_plug_DefaultMove( uint8 OnOff ) {
    uint8  newLevel;
    uint32 rate;      // fixed point decimal (3 places, eg. 16.345)
    uint16 time;

    // if moving to on position, move to on level
    if ( OnOff ) {
        if (zcl_lumi_plug_OnOff == LIGHT_OFF) {
            zcl_lumi_plug_LevelCurrentLevel = ATTR_LEVEL_MIN_LEVEL;
        }

        if ( zcl_lumi_plug_LevelOnLevel == ATTR_LEVEL_ON_LEVEL_NO_EFFECT ) {
            // The last Level (before going OFF) should be used)
            newLevel = zcl_lumi_plug_LevelLastLevel;
        } else {
            newLevel = zcl_lumi_plug_LevelOnLevel;
        }

        time = zcl_lumi_plug_LevelOnTransitionTime;

    } else {
        newLevel = ATTR_LEVEL_MIN_LEVEL;

        time = zcl_lumi_plug_LevelOffTransitionTime;
    }

    // else use OnOffTransitionTime if set (not 0xffff)
    if ( time == 0xFFFF ) {
        time = zcl_lumi_plug_LevelOnOffTransitionTime;
    }

    // else as fast as possible
    if ( time == 0xFFFF ) {
        time = 1;
    }

    // calculate rate based on time (int 10ths) for full transition (1-254)
    rate = 255000 / time;    // units per tick, fixed point, 3 decimal places (e.g. 8500 = 8.5 units per tick)

    // start up state machine.
    zcl_lumi_plug_WithOnOff = TRUE;
    zcl_lumi_plug_MoveBasedOnRate( newLevel, rate );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_AdjustLightLevel
 *
 * @brief   Called each 10th of a second while state machine running
 *
 * @param   none
 *
 * @return  none
 */
static void zcl_lumi_plug_AdjustLightLevel( void ) {
    // one tick (10th of a second) less
    if ( zcl_lumi_plug_LevelRemainingTime ) {
        --zcl_lumi_plug_LevelRemainingTime;
    }

    // no time left, done
    if ( zcl_lumi_plug_LevelRemainingTime == 0) {
        zcl_lumi_plug_LevelCurrentLevel = zcl_lumi_plug_NewLevel;
    }

    // still time left, keep increment/decrementing
    else {
        if ( zcl_lumi_plug_NewLevelUp ) {
            zcl_lumi_plug_CurrentLevel32 += zcl_lumi_plug_Rate32;
        } else {
            zcl_lumi_plug_CurrentLevel32 -= zcl_lumi_plug_Rate32;
        }
        zcl_lumi_plug_LevelCurrentLevel = (uint8)( zcl_lumi_plug_CurrentLevel32 / 1000 );
    }

    if (( zcl_lumi_plug_LevelChangeCmd == LEVEL_CHANGED_BY_LEVEL_CMD ) && ( zcl_lumi_plug_LevelOnLevel == ATTR_LEVEL_ON_LEVEL_NO_EFFECT )) {
        zcl_lumi_plug_LevelLastLevel = zcl_lumi_plug_LevelCurrentLevel;
    }

    // also affect on/off
    if ( zcl_lumi_plug_WithOnOff ) {
        if ( zcl_lumi_plug_LevelCurrentLevel > ATTR_LEVEL_MIN_LEVEL ) {
            zcl_lumi_plug_OnOff = LIGHT_ON;
        } else {
            if (zcl_lumi_plug_LevelChangeCmd != LEVEL_CHANGED_BY_ON_CMD) {
                zcl_lumi_plug_OnOff = LIGHT_OFF;
            } else {
                zcl_lumi_plug_OnOff = LIGHT_ON;
            }

            if (( zcl_lumi_plug_LevelChangeCmd != LEVEL_CHANGED_BY_LEVEL_CMD ) && ( zcl_lumi_plug_LevelOnLevel == ATTR_LEVEL_ON_LEVEL_NO_EFFECT )) {
                zcl_lumi_plug_LevelCurrentLevel = zcl_lumi_plug_LevelLastLevel;
            }
        }
    }

    zcl_lumi_plug_UpdateLedState();

    // display light level as we go
    UI_UpdateLcd( );

    // keep ticking away
    if ( zcl_lumi_plug_LevelRemainingTime ) {
        osal_start_timerEx( zcl_lumi_plug_TaskID, SAMPLELIGHT_LEVEL_CTRL_EVT, 100 );
    }
}

/*********************************************************************
 * @fn      zcl_lumi_plug_LevelControlMoveToLevelCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received a LevelControlMoveToLevel Command for this application.
 *
 * @param   pCmd - ZigBee command parameters
 *
 * @return  none
 */
static void zcl_lumi_plug_LevelControlMoveToLevelCB( zclLCMoveToLevel_t *pCmd ) {
    zcl_lumi_plug_LevelChangeCmd = LEVEL_CHANGED_BY_LEVEL_CMD;

    zcl_lumi_plug_WithOnOff = pCmd->withOnOff;
    zcl_lumi_plug_MoveBasedOnTime( pCmd->level, pCmd->transitionTime );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_LevelControlMoveCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received a LevelControlMove Command for this application.
 *
 * @param   pCmd - ZigBee command parameters
 *
 * @return  none
 */
static void zcl_lumi_plug_LevelControlMoveCB( zclLCMove_t *pCmd ) {
    uint8 newLevel;
    uint32 rate;

    // convert rate from units per second to units per tick (10ths of seconds)
    // and move at that right up or down
    zcl_lumi_plug_WithOnOff = pCmd->withOnOff;

    if ( pCmd->moveMode == LEVEL_MOVE_UP ) {
        newLevel = ATTR_LEVEL_MAX_LEVEL;  // fully on
    } else {
        newLevel = ATTR_LEVEL_MIN_LEVEL; // fully off
    }

    zcl_lumi_plug_LevelChangeCmd = LEVEL_CHANGED_BY_LEVEL_CMD;

    rate = (uint32)100 * pCmd->rate;
    zcl_lumi_plug_MoveBasedOnRate( newLevel, rate );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_LevelControlStepCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received an On/Off Command for this application.
 *
 * @param   pCmd - ZigBee command parameters
 *
 * @return  none
 */
static void zcl_lumi_plug_LevelControlStepCB( zclLCStep_t *pCmd ) {
    uint8 newLevel;

    // determine new level, but don't exceed boundaries
    if ( pCmd->stepMode == LEVEL_MOVE_UP ) {
        if ( (uint16)zcl_lumi_plug_LevelCurrentLevel + pCmd->amount > ATTR_LEVEL_MAX_LEVEL ) {
            newLevel = ATTR_LEVEL_MAX_LEVEL;
        } else {
            newLevel = zcl_lumi_plug_LevelCurrentLevel + pCmd->amount;
        }
    } else {
        if ( pCmd->amount >= zcl_lumi_plug_LevelCurrentLevel ) {
            newLevel = ATTR_LEVEL_MIN_LEVEL;
        } else {
            newLevel = zcl_lumi_plug_LevelCurrentLevel - pCmd->amount;
        }
    }

    zcl_lumi_plug_LevelChangeCmd = LEVEL_CHANGED_BY_LEVEL_CMD;

    // move to the new level
    zcl_lumi_plug_WithOnOff = pCmd->withOnOff;
    zcl_lumi_plug_MoveBasedOnTime( newLevel, pCmd->transitionTime );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_LevelControlStopCB
 *
 * @brief   Callback from the ZCL General Cluster Library when
 *          it received an Level Control Stop Command for this application.
 *
 * @param   pCmd - ZigBee command parameters
 *
 * @return  none
 */
static void zcl_lumi_plug_LevelControlStopCB( void ) {
    // stop immediately
    osal_stop_timerEx( zcl_lumi_plug_TaskID, SAMPLELIGHT_LEVEL_CTRL_EVT );
    zcl_lumi_plug_LevelRemainingTime = 0;
}
#endif

/*********************************************************************
 * @fn      zclSampleApp_BatteryWarningCB
 *
 * @brief   Called to handle battery-low situation.
 *
 * @param   voltLevel - level of severity
 *
 * @return  none
 */
void zclSampleApp_BatteryWarningCB( uint8 voltLevel ) {
    if ( voltLevel == VOLT_LEVEL_CAUTIOUS ) {
        // Send warning message to the gateway and blink LED
    } else if ( voltLevel == VOLT_LEVEL_BAD ) {
        // Shut down the system
    }
}

/******************************************************************************
 *
 *  Functions for processing ZCL Foundation incoming Command/Response messages
 *
 *****************************************************************************/

/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessIncomingMsg
 *
 * @brief   Process ZCL Foundation incoming message
 *
 * @param   pInMsg - pointer to the received message
 *
 * @return  none
 */
static void zcl_lumi_plug_ProcessIncomingMsg( zclIncomingMsg_t *pInMsg ) {
    switch ( pInMsg->zclHdr.commandID ) {
#ifdef ZCL_READ
    case ZCL_CMD_READ_RSP:
        zcl_lumi_plug_ProcessInReadRspCmd( pInMsg );
        break;
#endif
#ifdef ZCL_WRITE
    case ZCL_CMD_WRITE_RSP:
        zcl_lumi_plug_ProcessInWriteRspCmd( pInMsg );
        break;
#endif
    case ZCL_CMD_CONFIG_REPORT:
    case ZCL_CMD_CONFIG_REPORT_RSP:
    case ZCL_CMD_READ_REPORT_CFG:
    case ZCL_CMD_READ_REPORT_CFG_RSP:
    case ZCL_CMD_REPORT:
        //bdb_ProcessIncomingReportingMsg( pInMsg );
        break;

    case ZCL_CMD_DEFAULT_RSP:
        zcl_lumi_plug_ProcessInDefaultRspCmd( pInMsg );
        break;
#ifdef ZCL_DISCOVER
    case ZCL_CMD_DISCOVER_CMDS_RECEIVED_RSP:
        zcl_lumi_plug_ProcessInDiscCmdsRspCmd( pInMsg );
        break;

    case ZCL_CMD_DISCOVER_CMDS_GEN_RSP:
        zcl_lumi_plug_ProcessInDiscCmdsRspCmd( pInMsg );
        break;

    case ZCL_CMD_DISCOVER_ATTRS_RSP:
        zcl_lumi_plug_ProcessInDiscAttrsRspCmd( pInMsg );
        break;

    case ZCL_CMD_DISCOVER_ATTRS_EXT_RSP:
        zcl_lumi_plug_ProcessInDiscAttrsExtRspCmd( pInMsg );
        break;
#endif
    default:
        break;
    }

    if ( pInMsg->attrCmd )
        osal_mem_free( pInMsg->attrCmd );
}

#ifdef ZCL_READ
/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessInReadRspCmd
 *
 * @brief   Process the "Profile" Read Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zcl_lumi_plug_ProcessInReadRspCmd( zclIncomingMsg_t *pInMsg ) {
    zclReadRspCmd_t *readRspCmd;
    uint8 i;

    readRspCmd = (zclReadRspCmd_t *)pInMsg->attrCmd;
    for (i = 0; i < readRspCmd->numAttr; i++) {
        // Notify the originator of the results of the original read attributes
        // attempt and, for each successfull request, the value of the requested
        // attribute
    }

    return ( TRUE );
}
#endif // ZCL_READ

#ifdef ZCL_WRITE
/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessInWriteRspCmd
 *
 * @brief   Process the "Profile" Write Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zcl_lumi_plug_ProcessInWriteRspCmd( zclIncomingMsg_t *pInMsg ) {
    zclWriteRspCmd_t *writeRspCmd;
    uint8 i;

    writeRspCmd = (zclWriteRspCmd_t *)pInMsg->attrCmd;
    for ( i = 0; i < writeRspCmd->numAttr; i++ ) {
        // Notify the device of the results of the its original write attributes
        // command.
    }

    return ( TRUE );
}
#endif // ZCL_WRITE

/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessInDefaultRspCmd
 *
 * @brief   Process the "Profile" Default Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zcl_lumi_plug_ProcessInDefaultRspCmd( zclIncomingMsg_t *pInMsg ) {
    // zclDefaultRspCmd_t *defaultRspCmd = (zclDefaultRspCmd_t *)pInMsg->attrCmd;

    // Device is notified of the Default Response command.
    (void)pInMsg;

    return ( TRUE );
}

#ifdef ZCL_DISCOVER
/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessInDiscCmdsRspCmd
 *
 * @brief   Process the Discover Commands Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zcl_lumi_plug_ProcessInDiscCmdsRspCmd( zclIncomingMsg_t *pInMsg ) {
    zclDiscoverCmdsCmdRsp_t *discoverRspCmd;
    uint8 i;

    discoverRspCmd = (zclDiscoverCmdsCmdRsp_t *)pInMsg->attrCmd;
    for ( i = 0; i < discoverRspCmd->numCmd; i++ ) {
        // Device is notified of the result of its attribute discovery command.
    }

    return ( TRUE );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessInDiscAttrsRspCmd
 *
 * @brief   Process the "Profile" Discover Attributes Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zcl_lumi_plug_ProcessInDiscAttrsRspCmd( zclIncomingMsg_t *pInMsg ) {
    zclDiscoverAttrsRspCmd_t *discoverRspCmd;
    uint8 i;

    discoverRspCmd = (zclDiscoverAttrsRspCmd_t *)pInMsg->attrCmd;
    for ( i = 0; i < discoverRspCmd->numAttr; i++ ) {
        // Device is notified of the result of its attribute discovery command.
    }

    return ( TRUE );
}

/*********************************************************************
 * @fn      zcl_lumi_plug_ProcessInDiscAttrsExtRspCmd
 *
 * @brief   Process the "Profile" Discover Attributes Extended Response Command
 *
 * @param   pInMsg - incoming message to process
 *
 * @return  none
 */
static uint8 zcl_lumi_plug_ProcessInDiscAttrsExtRspCmd( zclIncomingMsg_t *pInMsg ) {
    zclDiscoverAttrsExtRsp_t *discoverRspCmd;
    uint8 i;

    discoverRspCmd = (zclDiscoverAttrsExtRsp_t *)pInMsg->attrCmd;
    for ( i = 0; i < discoverRspCmd->numAttr; i++ ) {
        // Device is notified of the result of its attribute discovery command.
    }

    return ( TRUE );
}
#endif // ZCL_DISCOVER

void zcl_lumi_plug_UiActionToggleLight(uint16 keys) {
    zcl_lumi_plug_OnOffCB(COMMAND_TOGGLE);
}

void zcl_lumi_plug_UpdateLedState(void) {
    // set the LED1 based on light (on or off)
    if ( zcl_lumi_plug_OnOff == LIGHT_ON ) {
        HalLedSet ( UI_LED_APP, HAL_LED_MODE_ON );
    } else {
        HalLedSet ( UI_LED_APP, HAL_LED_MODE_OFF );
    }
}

void zcl_lumi_plug_UiUpdateLcd(uint8 UiState, char * line[3]) {
#ifdef LCD_SUPPORTED
#ifdef ZCL_LEVEL_CTRL
    zclHA_uint8toa( zcl_lumi_plug_LevelCurrentLevel, &sLightLevel[9] );
    line[0] = (char *)sLightLevel;
#endif // ZCL_LEVEL_CTRL
    line[1] = (char *)(zcl_lumi_plug_OnOff ? sLightOn : sLightOff);
    line[2] = "< TOGGLE LIGHT >";
#endif
}

/****************************************************************************
****************************************************************************/


