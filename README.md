# zstack first sample

`Z-Stack 3.0 Sample Application User's Guide.pdf`->`1.2  Sample Projects ` 详细介绍了例程的功能，这里我们选择跑通`SampleLight / SampleSwitch `描述的功能，中间可能涉及led和key的驱动移植，同时通过抓包详细了解其网络建立、设备加入网络以及数据交互，至此，对zigbee和z-stack有个感性认识。

`Z-Stack 3.0 Sample Application User's Guide.pdf`->`3  Using the Sample Applications`->`3.2.1  SampleLight, SampleSwitch ` 详细介绍了该样例的功能。从描述来看，SampleLight 作为常供电设备扮演协调器角色，同时通过LED1用以指示灯开关，可以通过菜单导航，确认按键本地开关LED1，SampleSwitch因为是电池设备工作在终端设备模式，同时通过液晶的菜单导航能够远程开关 SampleLight的LED1。

* 烧写SampleLight 观测网络流程，并且移植按键和和可能的LED指示，通过按键能够翻转LED1；
* 烧写SampleSwitch 样例，观察设备加入流程，移植按键远程开关作为协调器的SampleLight的LED1；

## porting

* 为了方便抓包观察，更改SampleLight/SampleSwtich 样例中信道到一个干净信道，通过ubiqua信道扫描，这里选择17信道。

  ```c
  //f8wConfig.cfg line 45
  -DDEFAULT_CHANLIST=0x00020000  // 17 - 0x11
  ```

* 运行抓包，编译下载 SampleLight 运行程序；

  从抓包上来，这里没有任何数据，可能的网络的建立、设备加入网络都需要通过液晶交互吧。

* 移植LED/KEY用以网络状态指示；

  对比了网蜂`zigbee节点.pdf`和`swru208b_CC2530 Development Kit User’s Guide.pdf` 原理图确定LED指示灯是一一对应的，但是缺少LED4，对于按键smartrf05eb采用的5键摇杆，通过一个io通知按键事件发生，通过adc采样确定按键的值，由于我们底板只有两个按键，所以这里直接模拟5键摇杆的确认和右键。

  如下表所示为使用底板的外设接口：

  | item       | value            |
  | :--------- | ---------------- |
  | LED1、2、3 | P1_0、P1_1、P1_4 |
  | BTN1、2    | P0_4 P0_5        |
  
  增加按键定义
  
    ```c
    //Components/hal/target/CC2530EB/hal_key.c line 135
    /* s1 as joy stick  enter*/
    #define HAL_KEY_BTN_OK_PORT     P0
    #define HAL_KEY_BTN_OK_BIT      BV(0)
    #define HAL_KEY_BTN_OK_SEL      P0SEL
    #define HAL_KEY_BTN_OK_DIR      P0DIR
    
    /* s1 as joy stick  right*/
    #define HAL_KEY_BTN_MOVE_PORT   P0
    #define HAL_KEY_BTN_MOVE_BIT    BV(1)
    #define HAL_KEY_BTN_MOVE_SEL    P0SEL
    #define HAL_KEY_BTN_MOVE_DIR    P0DIR
    ```
  
  
  完成按键初始化
  
    ```c
    //Components/hal/target/CC2530EB/hal_key.c line 211  function.HalKeyInit
    
    HAL_KEY_BTN_OK_SEL &= ~(HAL_KEY_BTN_OK_BIT); /* Set pin function to GPIO */
    HAL_KEY_BTN_OK_DIR &= ~(HAL_KEY_BTN_OK_BIT); /* Set pin direction to Input */
    HAL_KEY_BTN_MOVE_SEL &= ~(HAL_KEY_BTN_MOVE_BIT); /* Set pin function to GPIO */
    HAL_KEY_BTN_MOVE_DIR &= ~(HAL_KEY_BTN_MOVE_BIT); /* Set pin direction to Input */
    ```
  
  修改按键功能,屏蔽原来摇杆adc采样功能，增加直接io输入
  
  
      ```c
      //  if ((HAL_KEY_JOY_MOVE_PORT & HAL_KEY_JOY_MOVE_BIT))  /* Key is active HIGH */
      //  {
      //    keys = halGetJoyKeyInput();
      //  }
      if (!(HAL_KEY_BTN_OK_PORT & HAL_KEY_BTN_OK_BIT)) { /* Key is active LOW */
          keys |= HAL_KEY_SW_5;
      } else if (!(HAL_KEY_BTN_MOVE_PORT & HAL_KEY_BTN_MOVE_BIT)) {
          keys |= HAL_KEY_SW_2;
      }
      ```


*  由于默认使能了网络安全功能，为了方便抓包分析，需要预定义网络密钥，否则会随时生成，导致抓包不能完成。

  ```bash
  //f8wConfig.cfg line 142
  /* -DDEFAULT_KEY="{0}" */
  -DDEFAULT_KEY="{0x06, 0x03, 0x06, 0x04, 0x09, 0x04, 0x02, 0x06, 0x04, 0x05, 0x04, 0x02, 0x05, 0x03, 0x05, 0x04}"
  ```

## test

参考如上步骤移植后就可以通过`Z-Stack 3.0 Sample Application User's Guide.pdf`->`7  Quick Start Guide` 测试观察现象了，因为没有液晶，所以这里需要阅读代码后盲测。大概流程如下：

> **提示**：完整固件保存在files/release_bin/*，完整抓包文件保存在 files/sniffer/*。

### sample light

* 编译SampleLight CoordinatorEB 并烧写到协调器，连续三次按下右键后(S2)，进入界面4—远程LED测试页面，通过确认按键（S1）可以连续开关LED1（红色）指示灯状态。

* 复位后，连续两次按下右键后(S2)进入界面3—网络测试页面，通过确认确认按键（S1）进入组网模式。

  ![协调器建立网络](images/coordinator_setup_network.png)

### sample switch

- 编译 SampleLight EndDeviceEB 并烧写到终端设备，连续两次（界面）按下右键后(S2)，通过确认（S1）按键进入LED测试页面，可以连续按键确认键翻转LED2（红色）指示灯状态。

- 复位后，连续两次按下右键后(S2)进入界面3—网络测试页面，通过确认确认按键（S1）进入组网模式。

  ![终端设备加入网络](images/sample_switch_join_network.png)

* 按一次右键，进入界面4—远程LED测试页面，，通过确认按键（S1）可以远程开关协调器LED1（红色）指示灯状态。

  ![远程开关操作](images/remote_toggle_on_off.png)

